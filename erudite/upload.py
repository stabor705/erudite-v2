from uuid import uuid4
from werkzeug.utils import secure_filename
import os

# Description: Upload file to server
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

def allowed_file(filename):
    """
    Check if file extension is allowed

    Parameters
    ----------
    filename : str
        File name
    
    Returns
    -------
    bool
        True if file extension is allowed, False otherwise
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def generate_filename(filename):
    """
    Generate unique file name
    
    Parameters
    ----------
    filename : str
        File name
    
    Returns
    -------
    str
        Unique file name
    """
    filename = secure_filename(filename)
    return str(uuid4()) + '.' + filename.rsplit('.', 1)[1].lower()


def save_file(file, filename):
    file.save(os.path.join(os.path.dirname(__file__), 'static', 'uploads', filename))