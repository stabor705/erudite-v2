from flask import Blueprint, Response, jsonify, request
from erudite.db import get_db
from erudite.parse import parse_id_from_query
from erudite.error import NotFoundException

bp = Blueprint('review', __name__, url_prefix='/reviews')


class ReviewNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("review", idx)

def fetch_review(review_id: int) -> dict:
    """
    Fetch a single review from database

    Parameters
    ----------
    review_id : int
        ID of review

    Returns
    -------
    dict
        dictionary representing fetched review
    """

    db = get_db()
    sql = """SELECT 
    id_review, content, id_user, id_book
    FROM review WHERE id_review = %s"""
    db.execute(sql, (review_id,))
    review = db.fetchone()

    if review is None:
        raise ReviewNotFoundException(review_id)
    return review

def fetch_reviews_for_book(id_book: int) -> list:
    """
    Fetch all reviews for a book from database

    Returns
    -------
    list
        list of dictionaries representing reviews
    """

    db = get_db()
    sql = """SELECT 
    id_review, content, id_user, id_book
    FROM review WHERE id_book = %s"""
    db.execute(sql, (id_book,))

    reviews = db.fetchall()

    return reviews

def fetch_reviews_for_user(id_user: int) -> list:
    """
    Fetch all reviews for a user from database

    Returns
    -------
    list
        list of dictionaries representing reviews
    """

    db = get_db()
    sql = """SELECT 
    id_review, content, id_user, id_book
    FROM review WHERE id_user = %s"""
    db.execute(sql, (id_user,))

    reviews = db.fetchall()

    return reviews

@bp.get("/<int:review_id>")
def get_review(review_id: int) -> Response:
    review = fetch_review(review_id)
    return jsonify(review)

@bp.get("/get_for_books")
def get_reviews_for_book() -> Response:
    id_book = parse_id_from_query("book", request.args)
    
    reviews = fetch_reviews_for_book(id_book)
    
    return jsonify(reviews)


@bp.get("/get_for_users")
def get_reviews_for_user() -> Response:

    id_user = parse_id_from_query("user", request.args)

    reviews = fetch_reviews_for_user(id_user)
    
    return jsonify(reviews)

