from flask import Blueprint, Response, jsonify, request
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth

bp = Blueprint('reservation', __name__, url_prefix='/reservations/')


def fetch_pending_reservations():
    db = get_db()
    sql = """
    SELECT id_reservation, receipt_term, submitted, id_catalog, id_user
    FROM reservation
    WHERE receipt_term IS NULL
    """
    db.execute(sql)
    return db.fetchall()

def get_reservations_for_user(user_id: int):
    db = get_db()
    sql = """
        SELECT id_reservation, receipt_term, submitted, id_catalog, id_user
        FROM reservation
        WHERE id_user = %s
        """
    db.execute(sql, (user_id, ))
    return db.fetchall()

@bp.get('/pending')
def get_pending_reservations_endpoint():
    reservations = fetch_pending_reservations()
    return jsonify(reservations)

@bp.get('/my_reservations')
@auth.login_required(role = 'client')
def get_reservations_for_user_endpoint():
    id_user = auth.current_user()["id_user"]
    reservations = get_reservations_for_user(id_user)
    return jsonify(reservations)
