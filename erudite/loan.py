from flask import Blueprint, Response, jsonify, request
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth
bp = Blueprint('loan', __name__, url_prefix='/loans')

class loanNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("loan", idx)

def fetch_loan(loan_id: int) -> dict:
    """
    Fetch a single loan from database

    Parameters
    ----------
    loan_id : int
        ID of loan

    Returns
    -------
    dict
        dictionary representing fetched loan
    """

    db = get_db()
    sql = """SELECT 
    id_loan, 
    to_char(date_of_loan, 'YYYY-MM-DD') as date_of_loan,
    to_char(date_of_return, 'YYYY-MM-DD') as date_of_return,
    prolongations, id_catalog, id_user
    FROM loan WHERE id_loan = %s"""
    db.execute(sql, (loan_id,))

    loan = db.fetchone()
    if loan is None:
        raise loanNotFoundException(loan_id)
    return loan

def fetch_pending_loans() -> list:
    """
    Fetch all pending(overdue) loans from database

    Returns
    -------
    list
        list of dictionaries representing pending loans
    """
    
    db = get_db()
    sql = """SELECT 
    id_loan,
    to_char(date_of_loan, 'YYYY-MM-DD') as date_of_loan,
    to_char(date_of_return, 'YYYY-MM-DD') as date_of_return, 
    prolongations, id_catalog, id_user
    FROM loan WHERE date_of_return < CURRENT_DATE"""
    db.execute(sql)

    return db.fetchall()

def fetch_loans_for_user(id_user: int) -> list:
    """
    Fetch all loans for a given user from database

    Parameters
    ----------
    id_user : int
        ID of user

    Returns
    -------
    list
        list of dictionaries representing loans for a given user

    """

    db = get_db()
    sql = """SELECT 
    id_loan,
    to_char(date_of_loan, 'YYYY-MM-DD') as date_of_loan,
    to_char(date_of_return, 'YYYY-MM-DD') as date_of_return, 
    prolongations, id_catalog, id_user
    FROM loan WHERE id_user = %s"""
    db.execute(sql, (id_user,))
    return db.fetchall()


def update_prolong(id_loan: int):
    db = get_db()
    loan = fetch_loan(id_loan)
    if loan is None:
        raise loanNotFoundException(id_loan)
    num = loan["prolongations"] + 1
    if num > 4:
        return "Limit of prolongations reached", 405
    else:
        sql = """
        UPDATE loan
        SET prolongations = prolongations + 1, date_of_return = date_of_return + interval '1 month'
        WHERE id_loan = %s
        """
        db.execute(sql, (id_loan,))
        return '', 204

@bp.get("/<int:loan_id>")
def get_loan(loan_id: int) -> Response:
    loan = fetch_loan(loan_id)
    return jsonify(loan)

@bp.get("/pending")
def get_pending_loans() -> Response:
    loans = fetch_pending_loans()
    return jsonify(loans)

@bp.get("/my_loans")
@auth.login_required(role = 'client')
def get_my_loans() -> Response:
    id_user = auth.current_user()["id_user"]
    loans = fetch_loans_for_user(id_user)
    return jsonify(loans)

@bp.post("/<int:loan_id>/prolong")
@auth.login_required(role = 'client')
def prolong_loan(loan_id: int):
    return update_prolong(loan_id)
