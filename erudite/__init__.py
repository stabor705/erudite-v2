import os

from flask import Flask
from flask_cors import CORS
from dotenv import load_dotenv

from . import error
from . import book, catalog, loan, review, publisher
from . import reservations
from . import auth
from . import author
from . import tag

load_dotenv()

def create_app(test_config=None):
    app = Flask(__name__)

    

    app.config.from_prefixed_env("ERUDITE")

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)
    auth.init_app(app)
    error.init_error_handling(app)

    app.register_blueprint(book.bp)
    app.register_blueprint(loan.bp)
    app.register_blueprint(review.bp)
    app.register_blueprint(publisher.bp)
    app.register_blueprint(catalog.bp)
    app.register_blueprint(reservations.bp)
    app.register_blueprint(author.bp)
    app.register_blueprint(tag.bp)
    app.register_blueprint(auth.bp)

    CORS(app)
    return app
