from flask import current_app, g
import psycopg
from psycopg.rows import dict_row
import click

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)

def init_db():
    with current_app.open_resource("schema.sql") as f:
        sql = f.read()

    with psycopg.connect(current_app.config['DATABASE']) as conn:
        with conn.cursor() as cur:
            cur.execute(sql)
            print(cur.statusmessage)

def get_db():
    if 'db' not in g:
        g.db = psycopg.connect(current_app.config['DATABASE'],
                               row_factory=dict_row,
                               autocommit=True).cursor()
    return g.db

def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

@click.command('init-db')
def init_db_command():
    init_db()
    click.echo('Initialized database')
