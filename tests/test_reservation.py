from flask.testing import FlaskClient
from auth import user

def test_getting_pending_reservations(client: FlaskClient):
    response = client.get('/reservations/pending')
    assert response.status_code == 200
    reservations = response.get_json()
    for reservation in reservations:
        assert reservation["receipt_term"] is None
        assert "id_reservation" in reservation
        assert "id_user" in reservation
        assert "id_catalog" in reservation
        assert "submitted" in reservation

def test_getting_reservations_for_user(client):
    response = client.get('/reservations/my_reservations', auth=user)
    reservations = response.get_json()
    i=0
    for reservation in reservations:
        i+=1
        assert reservation["id_user"] == 1
    assert i == 3
    assert response.status_code == 200