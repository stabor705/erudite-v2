from tests.auth import user

def test_unauthenticated_access(client):
    response = client.post('/publishers')
    assert response.status_code == 401

def test_unauthorized_access(client):
    response = client.post('/publishers', auth=user)
    assert response.status_code == 403

def test_auth_check_correct(client):
    response = client.get('/auth/check', auth=user)
    assert response.status_code == 200

def test_auth_check_no_auth(client):
    response = client.get('/auth/check')
    assert response.status_code == 401

def test_auth_check_wrong_credentials(client):
    response = client.get('/auth/check', auth=('thisuser', 'doesnotexist'))
    assert response.status_code == 401
