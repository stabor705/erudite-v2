from erudite.publisher import search_publisher_by_name
from tests.auth import librarian

def test_getting_publisher(client):
    response = client.get("/publishers/1")
    assert response.status_code == 200
    publisher = response.get_json()
    assert publisher["id_publisher"] == 1
    assert publisher["name"] == "SuperNOWA"
    
def test_getting_not_existing_publisher(client):
    response = client.get("/publishers/6")
    assert response.status_code == 404

def test_getting_books_for_publisher(client):
    response = client.get("/publishers/1/get_books")
    assert response.status_code == 200
    books = response.get_json()
    for book in books:
        assert book["publisher"] == {"id_publisher": 1, "name": "SuperNOWA"}
        assert len(book) == 9

def test_adding_publisher(client):
    publisher = {
        "name": "Rebis"
    }
    response = client.post('/publishers',json=publisher, auth=librarian)
    assert response.status_code == 204

def test_adding_invalid_publisher(client):
    publisher = {
        "duba": "Rebis"
    }
    response = client.post('/publishers',json=publisher, auth=librarian)
    assert response.status_code == 400

def test_searching_publishers_by_name(app):
    with app.app_context():
        publishers = search_publisher_by_name('supernowa')
    assert publishers["name"] == "SuperNOWA"
