--TODO: get rid of ids

INSERT INTO public.author(
	name)
VALUES ('Andrzej Sapkowski'),
	('Henryk Sienkiewicz'),
	('Adam Mickiewicz'),
	('Jason Schreier'),
	('Bolesław Prus'),
	('J.K. Rowling'),
	('Marcin Kosman');

INSERT INTO public.publisher(
	name)
	VALUES ('SuperNOWA'),
	('IBIS'),
	('SQN'),
	('Media Rodzina'),
	('Open Beta');

INSERT INTO public.book(
	title, cover, description, isbn, pages, year, id_publisher)
	VALUES ('Krew elfów', 'https://pictures.abebooks.com/isbn/9788370540791-us.jpg', 'Głównymi bohaterami powieści są Geralt z Rivii, wiedźmin, zawodowo trudniący się zabijaniem groźnych dla ludzi potworów, oraz królewna Ciri, nastoletnia dziewczyna, która w wyniku wojny utraciła rodzinę i tron.', '9788370540791', 295, 1994, 1),
	('Krew elfów', 'https://pictures.abebooks.com/isbn/9788375780307-us.jpg', 'Głównymi bohaterami powieści są Geralt z Rivii, wiedźmin, zawodowo trudniący się zabijaniem groźnych dla ludzi potworów, oraz królewna Ciri, nastoletnia dziewczyna, która w wyniku wojny utraciła rodzinę i tron.', '9788375780307', 295, 2011, 1),
	('Krzyżacy', 'https://staticl.poczytaj.pl/509000/krzyzacy-lektura-z-opracowaniem-sienkiewicz-henryk,509065-s.jpg', 'Powieść przedstawia dzieje konfliktu polsko-krzyżackiego, a akcja utworu toczy się od 1399 (rok śmierci królowej Jadwigi) do 1410 (bitwa pod Grunwaldem).', '9788366969568', 544, 2021, 2),
	('Pan Tadeusz, czyli ostatni zajazd na Litwie', 'https://staticl.poczytaj.pl/484000/pan-tadeusz-lektura-z-opracowaniem-adam-mickiewicz,484981-s.jpg', 'Opowiada o życiu polskiej szlachty pod zaborem rosyjskim i o przybyciu wojska napoleońskiego na Litwę.', '9788366729025', 400, 2020, 2),
	('Krew, pot i piksele. Chwalebne i niepokojące opowieści o tym, jak robi się gry', 'https://bonito.pl/cache/5/c1-krew-pot-i-piksele-chwale_400.jpg', 'Opisując artystyczne wyzwania stojące przed producentami, techniczne bariery, żądania graczy i pracę przypominającą robotę w korporacji, autor dochodzi do wniosku, że każda z gier jest swoistym cudem.', '9788381292436', 336, 2018, 3),
	('Lalka', 'https://multiszop.pl/Photos/9/LARGE/000066/lalka-lektura-z-opracowaniem-zlota-seria.jpg', 'Bohaterem Lalki jest Stanisław Wokulski, człowiek o dwóch obliczach. Z jednej strony mocno stąpający po ziemi racjonalista, z drugiej romantycznie zakochany idealista.', '9788365634498', 768, 2016, 2),
	('Harry Potter i wiezień Azkabanu', 'https://bonito.pl/cache/9/0ee84aedcf1a8eea13545d72a9a5b89d_400.jpg', 'Fabuła tomu skupia się na zbiegu z Azkabanu − Syriuszu Blacku − niesłusznie oskarżonym przestępcą, który siedział w więzieniu dwanaście lat i z niego uciekł. Jest ojcem chrzestnym Harry''ego Pottera.', '9788380082151', 454, 2020, 4),
	('Dziady', 'https://bonito.pl/zdjecia/6/017783a-dziady-lektura-z-opr.jpg', 'Osobiste przeżycia i aktualne wydarzenia polityczne (śledztwo i aresztowania wśród filomatów oraz martyrologia młodzieży polskiej), przeniósł poeta na płaszczyznę eschatologiczną odwiecznych zmagań dobra i zła oraz relacji świata żywych i świata zmarłych.', '9788366969735', 256, 2021, 2),
	('Nie tylko Wiedźmin. Historia polskich gier komputerowych', 'https://emp-scs-uat.img-osdw.pl/img-p/1/kipwn/c0aac775/std/e6-172/108794320o.jpg', 'Opowieść o ludziach i ich wirtualnych dziełach. Zaczyna się w mrocznych czasach PRL-u, gdy władza nie dbała o prawa autorskie, a producentom sen z powiek spędzały takie problemy, jak zakup czystych kaset magnetofonowych, na które można było nagrywać gry.', '9788394162504', 400, 2015, 5);

INSERT INTO public.book_author(
	id_book, id_author)
	VALUES (1, 1),
	(2, 1),
	(3, 2),
	(4, 3),
	(5, 4),
	(6, 5),
	(7, 6),
	(8, 3),
	(9, 7);

INSERT INTO public.catalog(
	id_book, shelf_mark)
	VALUES (1, 'A1'),
	(2, 'A1'),
	(3, 'A4'),
	(4, 'C6'),
	(5, 'A5'),
	(6, 'B9'),
	(7, 'C8'),
	(8, 'C6'),
	(8, 'C10'),
	(9, 'B1');

INSERT INTO public.users(
	name, password, email, birth_date, avatar, description, role)
	VALUES ('Hubert Asztabski', '\x136eb5f51373a1246b3e5efdb17d9fd405cd1bc3de0ea44d417aa5c7534150da21e2894a1d62252c80c3db954a73e11d7900d519596f363d788bdf0695ebd547', 'hubertasztabski@gmail.com', '2002-10-24', null, 'Filozof, filantrop, pasjonat i hobbysta książek', 'client'),
	('Stanisław Borowy', '\x263e5df430319478d8e3270026b2c960e43a9f7a1d6e2a7ac13f96ea10fc335a64aa5be1bdb518c1c2fbcc687627ba995ff2007ebe0cfd53abcf719e4949aac1', 'stanislawborowy@onet.pl', '1969-11-11', null, 'Wielki Polak i wielki czytelnik od ponad 50 lat. Niech żyje Jan Paweł II.', 'client'),
	('Jakub Głowacki', '\xa79e8da716fcdb8660c09d80e68fed66008d3b87137bf91afef32129d05e2aaca54535481a9546f4883971b0ac08f2211c067e97dc63533310db9190640ef667', 'jakubglowacki@yahoo.com', '2001-12-04', null, 'IT Student, Gamer, Chef, Baker', 'client'),
	('Jan Karkowski', '\xcf541956441b37b3d98d61cbff0028c0d3bcb05e5f1c150e3cf4e721fdd85f32cb5e6ba7082b70eac772974630ec3acbd8c70b7f120af2c82de0d30b43840fa0', 'jankarkowski@gmail.com', '2002-7-27', null, '"Będę grał w grę. *jaką?* TOMB RAJDER" - Bartek z Gliwic', 'client'),
	('Agnieszka Dzwon', '\x48018085695d9b65440ac24036ae41293f6b695dd6e7cdc9eb8d728d89998fe9b129500873ee412c5d930bd6aa3c15bdb2cc7bac3825297f41e75a8729dd1d46', 'agnieszka1987@gmail.com', '1987-2-13', null, 'Najlepsza bibliotekarka 9 miesięcy z rzędu', 'librarian');

INSERT INTO public.loan(
	id_user, id_catalog, date_of_loan, date_of_return, prolongations)
	VALUES (2, 6, '2022-12-02', '2023-3-02', 4),
	(1, 5, '2022-12-03', '2023-3-03', 0),
	(4, 7, '2022-12-01', '2023-3-01', 2);

INSERT INTO public.reservation(
	id_user, id_catalog, submitted, receipt_term)
	VALUES (2, 3, '2022-11-27', '2022-12-04'),
	(1, 1, '2022-11-25', NULL),
	(1, 4, '2022-11-29', '2022-12-06'),
	(1, 9, '2022-12-02', NULL);

INSERT INTO public.review(
	content, id_user, id_book)
	VALUES ('Fajne. Bardzo fajne.', 2, 6),
	('Dawno nie czytałem tak niesamowicie głupiej książki.', 4, 7),
	('Wielokrotnie się słyszy, że gry to "zło wcielone" ale ta książka pokazuje iż gry to ludzie je tworzący...czyli ludzie z pasją, wizją i potencjałem, a same gry nie są złe. Warto przeczytać by wiedzieć o co chodzi w tych grach.', 1, 5);

INSERT INTO public.tag(
	color, name)
	VALUES ('#32CD32', 'fantasy'),
	('#8B4513', 'lektury'),
	('#DC143C', 'literatura polska'),
	('#1E90FF', 'literatura faktu'),
	('#9ACD32', 'przygodowa'),
	('#FF8C00', 'literatura dla młodzieży'),
	('#40E0D0', 'popularnonaukowa');

INSERT INTO public.book_tag(
	id_book, id_tag)
	VALUES (1, 1),
	(1, 3),
	(1, 5),
	(2, 1),
	(2, 3),
	(2, 5),
	(3, 2),
	(3, 3),
	(4, 2),
	(4, 3),
	(5, 4),
	(5, 7),
	(6, 2),
	(6, 3),
	(7, 1),
	(7, 5),
	(7, 6),
	(8, 2),
	(8, 3),
	(9, 3),
	(9, 4),
	(9, 7);
